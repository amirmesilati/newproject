import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/** @title Simple form field */

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  cities:object[] = [{id:1 , name:'Tel Aviv'},{id:1 , name:'Berlin'},{id:1 , name:'Paris'},{id:1 , name:'London'},{id:1 , name:'null'}];
  city:string;
  temperatureTempForm:string;

onSubmit(){
  this.router.navigate(['/temperatures',this.city])
}

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
