export interface BlogPosts {
    userId:Number,
    id:Number,
    title:String,
    body:String
}
