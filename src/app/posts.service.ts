import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BlogPosts} from './interfaces/blog-posts';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private APIPosts = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient,private db: AngularFirestore) { }

  getBlogPosts():Observable<BlogPosts[]>{
    return this.http.get<BlogPosts[]>(`${this.APIPosts}`);
  }
  
  addPosts_To_Firebase(title:String, body:String,id_post:Number){ 
    console.log('in add posts')
    const post = {body:body,title:title,id:id_post}
    this.db.collection('posts').add(post);
            }
}
