import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../posts.service';
import { BlogPosts } from '../interfaces/blog-posts';
import { Router , ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
  posts$:BlogPosts[]=[];
  title:String; 
  body:String;
  id:Number=1
  message:String;


  
  constructor(private postsService:PostsService,private router:Router) { }

  ngOnInit() {
    this.postsService.getBlogPosts().subscribe(posts_form_API_servic_observable => this.posts$ = posts_form_API_servic_observable);
    this.postsService.getBlogPosts().subscribe(data =>this.posts$ = data);
  }

  saveFunc(){
    for (let index = 1; index < this.posts$.length; index++) {
        if (this.posts$[index].id==this.posts$[index].id) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.id = this.posts$[index].id;
          this.postsService.addPosts_To_Firebase(this.body,this.title,this.id);
          }
          this.message ="The data loading was successful"
      }
      
   
  }
  
}
