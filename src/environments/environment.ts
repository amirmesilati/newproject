// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
  apiKey: "AIzaSyArK5FKD-OlpBjc_vlFJ723Rpandk4SCic",
  authDomain: "newproject-5a198.firebaseapp.com",
  databaseURL: "https://newproject-5a198.firebaseio.com",
  projectId: "newproject-5a198",
  storageBucket: "newproject-5a198.appspot.com",
  messagingSenderId: "581817564397",
  appId: "1:581817564397:web:f06c1edf984e83681820e4",
  measurementId: "G-ST3B04EJLM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
